Kubernetes Watcher Scripts
---

Set of watcher scripts to watch for events in kubernetes

This repo will collect a set of watcher scripts designed to
watch for events in a kubernetes cluster.

### Apis

This repo includes examples using the following apis:
- [python](https://github.com/kubernetes-client/python/blob/master/kubernetes/README.md)
- [openshift](https://github.com/openshift/openshift-restclient-python)

### Dockerfile

Builds a container image from python 3.9
installs kubernetes, kubectl, and jq

```
docker build -t k8s_watcher .
```

### Examples

All require a few environment variables to work
- HELM_TOKEN: token to access the api. This can be from a service_account, or
copied from the ocp console ui for your user
- CLUSTER_SERVER: url to the cluster

#### watch_services_namespace.py

takes a namespace, and requires HELM_TOKEN environment variable with
a token for a service account with access to the namespace.

Prints information about all service events (Add, Delete, etc.) detected

If you have a file `namespace.login.env` with HELM_TOKEN and CLUSTER_SERVER environment variables:
```
 docker run -ti --rm --env-file namespace.login.env -v`pwd`:/watcher --workdir /watcher -u $(id -u):$(id -g) watcher bash
 ./watch_services_namespace somenamespace
 ```

do some things that create/delete services in the namespace, such as
running a deployment. Note, it will show events for things that already
exist before showing events for new things.

#### bad_deployments.py

takes a namespace, and reports any deployment event where the number of
specified replicas does not equal the actual replicas

#### kubectl login

mostly for testing connection to the CLUSTER_SERVER using the HELM_TOKEN. Also
requires a PROJECT_NAMESPACE with the namespace you are accessing, and
the HELM_USER for the HELM_TOKEN.

#### namespace_info.py

Takes a namespace and returns information about the namespace

#### pod_backoff_watcher.py

Takes a namespace and pod name, watches for Crashloop Backoff events for
the pod in the namespace

#### pod_watcher.py

takes a namespace and name, and prints the status of the pod any time
it changes

#### openshift_list_services.py

takes a namespace, and lists the services in that namespace using
the openshift dynamic client

#### show_me_the_architecture.py

takes a namespace, and prints out a [plantuml](https://plantuml.com) architecture
description of each application (deployment, or statefulste), service, and route,
with mappings of each route to its target service and port, and each service
to its target container and port.
