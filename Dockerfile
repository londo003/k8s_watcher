FROM python:3.9.2

ENV APP_HOME /watchers

ENV JQ_VERSION=1.6
ENV KUBECTL_VERSION=v1.12.8

RUN apt-get update -qq \
    && apt-get upgrade -y \
    && pip install kubernetes openshift \
    && curl -L -s -S https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -o /usr/local/bin/jq \
    && chmod +x /usr/local/bin/jq \
    && curl -L -s -S https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && mkdir /.kube && chmod -R 777 /.kube \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives

ADD examples ${APP_HOME}/examples
RUN chmod -R g=rwX ${APP_HOME}

WORKDIR ${APP_HOME}
