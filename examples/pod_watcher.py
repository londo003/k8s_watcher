#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config, watch
from pprint import pprint

if len(sys.argv) < 3:
    print("usage: %s namespace name" % (sys.argv[0]))
    exit()

namespace = sys.argv[1]
name = sys.argv[2]

try:
    config.load_incluster_config()
except:
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    client.Configuration.set_default(configuration)

timeout_seconds = 60 * 2 # 2 hours
v1 = client.CoreV1Api()
w = watch.Watch()
for event in w.stream(v1.list_namespaced_pod, namespace=namespace):
    this_pod = event['object']
    if this_pod.metadata.name == name:
        print("%s pod has status %s" % (this_pod.metadata.name, this_pod.status.phase))