#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config, watch
from pprint import pprint

if len(sys.argv) < 3:
    print("usage: %s namespace name" % (sys.argv[0]))
    exit()

namespace = sys.argv[1]
name = sys.argv[2]

try:
    config.load_incluster_config()
except:
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    client.Configuration.set_default(configuration)

timeout_seconds = 60 * 2 # 2 hours
v1 = client.CoreV1Api()
w = watch.Watch()
for event in w.stream(v1.list_namespaced_event, namespace=namespace):
    if event['object'].involved_object.name == name:
        if event['object'].reason == 'BackOff':
            print("Pod %s Restarted %s" % (event['object'].involved_object.name, event['object'].message))
