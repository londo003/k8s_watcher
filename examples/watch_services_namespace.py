#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config, watch

if len(sys.argv) < 2:
    print("usage: %s namespace" % (sys.argv[0]))
    exit()

namespace = sys.argv[1]

try:
    config.load_incluster_config()
except:
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    client.Configuration.set_default(configuration)

timeout_seconds = 60 * 2 # 2 hours
v1 = client.CoreV1Api()
w = watch.Watch()
for event in w.stream(v1.list_namespaced_service, namespace=namespace, timeout_seconds=timeout_seconds):
    #print("Event: %s %s" % (event['type'], event['object'].metadata.name))
    print("Detected %s of %s named %s in namespace %s" % (event['type'], event['object'].kind, event['object'].metadata.name, event['object'].metadata.namespace))
    print("labels:")
    for k, v in event['object'].metadata.labels.items():
        print("    %s %s" % (k,v))
    print("ports:")
    for port in event['object'].spec.ports:
        print("    %s" % (port.port))
    if event['object'].spec.selector != None:
        print("pod selector: ")
        for k, v in event['object'].spec.selector.items():
            print("    %s %s" % (k, v))
 