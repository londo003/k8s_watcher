#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config
from openshift.dynamic import DynamicClient
from pprint import pprint

if len(sys.argv) < 2:
    print("usage: %s namespace" % (sys.argv[0]))
    exit()

namespace_name = sys.argv[1]

try:
    k8s_client = config.new_client_from_config()
except:
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    k8s_client = client.ApiClient(configuration)

dyn_client = DynamicClient(k8s_client)
v1_services = dyn_client.resources.get(api_version='v1', kind='Service')
services = v1_services.get(namespace=namespace_name)
pprint(services)