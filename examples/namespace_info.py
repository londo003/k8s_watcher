#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config
from pprint import pprint

if len(sys.argv) < 2:
    print("usage: %s namespace" % (sys.argv[0]))
    exit()

namespace_name = sys.argv[1]

try:
    config.load_incluster_config()
except:
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    client.Configuration.set_default(configuration)

v1 = client.CoreV1Api()
namespace = v1.read_namespace(namespace_name)
pprint(namespace)
