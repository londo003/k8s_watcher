#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config
from openshift.dynamic import DynamicClient
from pprint import pprint

if len(sys.argv) < 2:
    print("usage: %s namespace" % (sys.argv[0]))
    exit()

namespace_name = sys.argv[1]

def get_owner(owned):
    owner = owned.metadata.ownerReferences[0]
    v1_this = dyn_client.resources.get(api_version=owner.apiVersion, kind=owner.kind)
    return v1_this.get(name=owner.name, namespace=namespace_name)

def application_uml(app):
    plantuml_out.write(f'{indent}package "{app.metadata.name}-{app.kind.lower()}"')
    plantuml_out.write(' {\n')
    for container in app.spec.template.spec.containers:
        cname = container.name.replace('-','_')
        plantuml_out.write(f'{indent}{indent} object {cname}_container')
        plantuml_out.write('\n')
    plantuml_out.write(indent)
    plantuml_out.write('}\n')

try:
    k8s_client = config.new_client_from_config()
except:
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    k8s_client = client.ApiClient(configuration)

dyn_client = DynamicClient(k8s_client)

services = {}
routes = {}

plantuml_out = open(f'{namespace_name}_architecture.plantuml', "w")

indent = '    '
plantuml_out.write('@startuml\n')
plantuml_out.write(f'{indent}left to right direction\n')
plantuml_out.write(f'{indent}object external_world\n')

v1_deployments = dyn_client.resources.get(api_version='v1', kind='Deployment')
deployment_list = v1_deployments.get(namespace=namespace_name)
for dep_obj in deployment_list.items:
    application_uml(dep_obj)

v1_statefulsets = dyn_client.resources.get(api_version='v1', kind='StatefulSet')
statefulset_list = v1_statefulsets.get(namespace=namespace_name)
for ss_obj in statefulset_list.items:
    application_uml(ss_obj)
plantuml_out.write('\n')

v1_services = dyn_client.resources.get(api_version='v1', kind='Service')
v1_pods = dyn_client.resources.get(api_version='v1', kind='Pod')

service_list = v1_services.get(namespace=namespace_name)
for service_obj in service_list.items:
    # only write services with selectors to application pods
    selects_application = False

    if service_obj.spec.selector:
        pod_selector_label = ','.join('{}={}'.format(k,v) for (k,v) in service_obj.spec.selector)
        pod_list = v1_pods.get(namespace=namespace_name,label_selector=pod_selector_label)
        for pod_obj in pod_list.items:
            selects_application = True

            this_obj = get_owner(pod_obj)
            if this_obj.kind == 'StatefulSet':
                services[service_obj.metadata.name] = {
                    'ports': {s.name: s for s in service_obj.spec.ports},
                    'containers': {}
                    
                }
                for container in this_obj.spec.template.spec.containers:
                    for container_port in container.ports:
                        for service_port in service_obj.spec.ports:
                            if service_port.targetPort == container_port.containerPort:
                                services[service_obj.metadata.name]['containers'][service_port.targetPort] = f'{container.name.replace("-","_")}_container : {service_port.targetPort}'
            else:
                this_obj2 = get_owner(this_obj)
                services[service_obj.metadata.name] = {
                    'ports': {s.name: s for s in service_obj.spec.ports},
                    'containers': {}
                    
                }
                for container in this_obj2.spec.template.spec.containers:
                    for container_port in container.ports:
                        for service_port in service_obj.spec.ports:
                            if service_port.targetPort == container_port.containerPort:
                                services[service_obj.metadata.name]['containers'][service_port.targetPort] = f'{container.name.replace("-","_")}_container : {service_port.targetPort}'
    if selects_application:
        service_name = service_obj.metadata.name.replace('-','_')
        plantuml_out.write(f'{indent}object {service_name}_{service_obj.kind.lower()}')
        plantuml_out.write('\n')
plantuml_out.write('\n')

v1_routes = dyn_client.resources.get(api_version='v1', kind='Route')
route_list = v1_routes.get(namespace=namespace_name)
for route_obj in route_list.items:
    route_name = f'{route_obj.metadata.name.replace("-","_")}_{route_obj.kind.lower()}'
    plantuml_out.write(f'{indent}object {route_name}')
    plantuml_out.write('\n')
    plantuml_out.write(f'{indent}{route_name} : host = "https://{route_obj.spec.host}"')
    plantuml_out.write('\n')

    target_service = services[route_obj.spec.to.name]
    target_service_name = f'{route_obj.spec.to.name.replace("-","_")}_service'
    target_port = target_service['ports'][route_obj.spec.port.targetPort].port
    routes[route_name] = f'{target_service_name} : {target_port}'
plantuml_out.write('\n')

for route_key in routes:
    plantuml_out.write(f'{indent}external_world --> {route_key} : https')
    plantuml_out.write('\n')
plantuml_out.write('\n')

for service_real, service in services.items():
    service_name = f'{service_real.replace("-","_")}_service'
    for target_port, container_mapping in service['containers'].items():
        plantuml_out.write(f'{indent}{service_name} --> {container_mapping}')
        plantuml_out.write('\n')
plantuml_out.write('\n')

for route_name, route in routes.items():
    plantuml_out.write(f'{indent}{route_name} --> {route}')
    plantuml_out.write('\n')
plantuml_out.write('\n')

plantuml_out.write('@enduml\n')
plantuml_out.close()