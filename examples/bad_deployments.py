#!/usr/local/bin/python

import os
import sys
from kubernetes import client, config, watch
from pprint import pprint

try:
    config.load_incluster_config()
except:
    if len(sys.argv) < 2:
        print("usage: %s namespace" % (sys.argv[0]))
        exit()

    namespace = sys.argv[1]
    configuration = client.Configuration()
    configuration.api_key={"authorization":"Bearer "+ os.getenv('HELM_TOKEN')}
    configuration.host = os.getenv('CLUSTER_SERVER')
    client.Configuration.set_default(configuration)

timeout_seconds = 60 * 2 # 2 hours
v1 = client.AppsV1Api()
w = watch.Watch()
for event in w.stream(v1.list_namespaced_deployment, namespace=namespace, timeout_seconds=timeout_seconds):
    this_deployment = event['object']
    if this_deployment.spec.replicas != this_deployment.status.available_replicas:
        print("Deployment %s in namespace %s is bad" % (this_deployment.metadata.name, this_deployment.metadata.namespace))
